# Default .bash_profile for MNT Reform

export PATH=$PATH:$HOME/bin

# enable NIR shader path in mesa. without this,
# some Xwayland applications will be blank
export ETNA_MESA_DEBUG=nir

# set GTK2 theme
export GTK2_RC_FILES=/usr/share/themes/Arc-Dark/gtk-2.0/gtkrc

unicode_start

if [ $(whoami) == "root" ]
then
   cat /etc/reform-root-help
elif [ ! -z $WAYLAND_DISPLAY ]
then
    # do nothing
    true
else
   cat /etc/reform-help
fi
