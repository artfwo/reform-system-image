#!/bin/bash

set -x
set -e

export ARCH=arm64
#export LOADADDR=0x40480000
export CROSS_COMPILE=aarch64-linux-gnu-

if [ ! -d linux ]
then
  echo "Cloning Linux..."
  mkdir linux
  cd linux
  git init
  git remote add origin https://github.com/torvalds/linux.git
  # temporary linux 5.11rc7 commit
  git fetch --depth 1 origin e0756cfc7d7cd08c98a53b6009c091a3f6a50be6
  git checkout FETCH_HEAD
  cd ..
fi

cp ./template-kernel/*.dts ./linux/arch/arm64/boot/dts/freescale/
cp ./template-kernel/kernel-config ./linux/.config

cd linux

for PATCHFILE in ../template-kernel/patches/*.patch
do
  echo PATCH: $PATCHFILE
  if git apply --check $PATCHFILE; then
    git apply $PATCHFILE
  else
    echo "\e[1mKernel patch already applied or cannot apply: $PATCHFILE"
  fi
done

make -j$(nproc) Image freescale/imx8mq-mnt-reform2.dtb freescale/imx8mq-mnt-reform2-hdmi.dtb

cd ..
